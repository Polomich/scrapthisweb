# -*- coding: utf-8 -*-
"""
@author: Rosemary Canon
"""
from urllib.request import urlopen, Request
import fileIO_utils as IO
from bs4 import BeautifulSoup as BS
import re
import copy
from datetime import datetime, timedelta


def archive_data_from_webpage():
    """Scrapes a given website then saves it as a local html file.

    Returns
    -------
    None.

    """
    # We should verify if a file for the current date has been already been created. 
    # This will prevent from scraping the webpage again if a file was already created for the current date.
    
    if IO.verify_existing_file() is None:
        url = "https://www.worldometers.info/coronavirus/"
        request = Request(url, headers={'user-agent':'Mozilla/5.0'})
        html_code = urlopen(request)
        code = html_code.read()
        
        # save the file once scraped
        IO.save_to_local_file(code)
        

class Scraped_Data:
    def __init__(self, url):
        self.__url = url
        self.__today_date = None
        self.__yesterday_date = None
        self.__yesterday2_date = None
        self.__bs_obj = self.__get_bs_obj(url)
        self.__today_data = self.__set_today_data()
        self.__yesterday_data = self.__set_yesterday_data()
        self.__yesterday2_data = self.__set_yesterday2_data()

    def __scrape_data_into_list(self, table_rows, date):
        """ Extracts the data from the local file. This function will return
            a list of tuples that will be used to populate a database table.
    
        Parameters
        ----------
        table_rows : 
            Table rows from a BeautifulSoup object.
    
        Returns
        -------
        None.
    
        """
        
        # all rows are within the table body
        tbody_rows = table_rows.find('tbody')
       
        # we need to get the rows that are needed 
        rows = self.__remove_unwanted_rows(tbody_rows)
        
        # extra the data for each row and store them in a list of tuples
        clean_vals = self.__clean_values(rows, date) 
        
        # print(clean_vals[0])
        return clean_vals

    def __remove_unwanted_rows(self, tbody_rows):
        """ Removes all unwanted rows from the table body. Table rows (<tr>) elements 
        containing a style attribute with an empty value or a background-color are preserved.
    
        Parameters
        ----------
        tbody_rows : bs4.element.Tag
            BeautifulSoup object containing table rows.
    
        Returns
        -------
        clean_data : list
            List containing valid rows.
    
        """
        
        clean_data = []
        for table_row in tbody_rows.children:
            if table_row == "\n":
                continue
            elif table_row.get('style') in ['background-color:#EAF7D5', 'background-color:#F0F0F0', '' ]:
                clean_data.append(table_row)
    
        return clean_data
     
    def __clean_values(self, rows, date):
        """Cleans the values within each row.
    
        Parameters
        ----------
        rows : list
            List of bs4 element tags (table rows)
            
        date : str
            Date that will be used as a first element within each tuple in the returned list.
    
        Returns
        -------
        clean_values : list
            List of tuples containing the cleaned data for each table row.
    
        """
        
        # This will contain tuples containing the table data values of each table row.
        clean_values = []
        for row in rows:
            clean_row_data = [date]
            
            # This will contain all the table data for the row.
            data = row.find_all('td')
            
            for d in data:
                if d.text == "N/A":
                    clean_row_data.append('0')
                    continue
                elif re.match("\s", d.text):
                    clean_row_data.append('0')
                    continue
                elif d.text != "":
                    value = re.sub("[+,]", "", d.text).strip()
                    value = value.replace(" ", "")
                    clean_row_data.append(value)
                    continue
                clean_row_data.append('0')
            
            # Append the list as a tuple into the clean_values list.
            clean_values.append(tuple(clean_row_data))
                
        return clean_values

    def __get_bs_obj(self, local_url):
        """ Creates and returns a BeautifulSoup object given a path of an hmtl file.

        Parameters
        ----------
        local_url : str
            Path of the local file.

        Returns
        -------
        BeautifulSoup 
            A BeautifulSoup object of the given html file.

        """
        html_bytes = urlopen("file:local_html/" + local_url)
        return BS(html_bytes, features="html.parser")

    def __set_today_data(self):
        """ Sets the private member today_data.
        
        Returns
        -------
        List
            List of tuples containing data for today_date.

        """
        table_rows = self.__bs_obj.find(id="main_table_countries_today")
        # Sets the dates for each of the table
        self.__get_date_from_url()
        return self.__scrape_data_into_list(table_rows, self.__today_date)
    
    def __set_yesterday_data(self):
        """ Sets the private member today_data.
        
        Returns
        -------
        List
            List of tuples containing data for yesterday_date.

        """
        table_rows = self.__bs_obj.find(id="main_table_countries_yesterday")
        return self.__scrape_data_into_list(table_rows, self.__yesterday_date)
    
    def __set_yesterday2_data(self):
        """ Sets the private member today_data.
        
        Returns
        -------
        List
            List of tuples containing data for yesterday2_date.

        """
        table_rows = self.__bs_obj.find(id="main_table_countries_yesterday2")
        return self.__scrape_data_into_list(table_rows, self.__yesterday2_date)
    
    def __get_date_from_url(self):
        """Extracts the date within the given filename (url). The date is then
        used to get the dates for the two previous days. Each of the dates are 
        used to initialize the respective private members (today_date, yesterday_date
        and yesterday2_day).
        
        
        Returns
        -------
        None.

        """
        # Gets the date tagged in the url. The date has a format of YYYY-MM-DD.
        reg = re.compile('\d{4}-\d{2}-\d{2}')
        # findall will only return 1 value, therefore using [0] to access it.
        date = reg.findall(self.__url)[0]
        
        # Used to transform into datetime obj, this is needed to get
        # the dates for yesterday and yesterday2.
        date_obj = datetime.date(datetime.strptime(date, "%Y-%m-%d"))
        
        # These will represent the days between the current date, yesterday and yesterday2.
        days_yesterday = timedelta(1)
        days_yesterday2 = timedelta(2)
        
        # Set the private members.
        self.__today_date = date
        self.__yesterday_date = str(date_obj - days_yesterday)
        self.__yesterday2_date = str(date_obj - days_yesterday2)

    def get_data(self):
        """ Returns the data for all 3 days within a tuple.

        Returns
        -------
        list
            List containing tuples of data for the 3 days.

        """
        
        list1 = copy.deepcopy(self.__today_data)
        list2 = copy.deepcopy(self.__yesterday_data)
        list3 = copy.deepcopy(self.__yesterday2_data)
        return list1 + list2 + list3        
