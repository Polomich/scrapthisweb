# -*- coding: utf-8 -*-
"""
@author: Rosemary Canon and Michelle Polonsky
"""
from datetime import datetime, timedelta
from os import scandir


def save_to_local_file(html_bytes):
    """Saves html bytes into a local html files. All files will be saved in a directory
    named local_html within the current working directory.

    Parameters
    ----------
    html_bytes : bytes
        HTML bytes to save within local file

    """

    # The filename will be tagged with the current date when the file is created.
    date_str_format = str(datetime.date(datetime.now()))

    filename = "local_page" + date_str_format + ".html"

    # Prepend the directory name.
    file_path = "local_html/" + filename

    # Write the bytes into a file
    file = open(file_path, 'wb')
    file.write(html_bytes)
    file.close()
    print(filename + " successfully saved.")


def verify_existing_file(date=None):
    """Verifies if a file for a given date exists.

    Parameters
    ----------
    date : str, optional
        the day searched. If not provided, it defaults to verifying if a file was created for the current date.

    Returns
    -------
    filename : str
        Name of file tagged with a given date.

    """

    if date is None:
        # The filename will be tagged with the current date when the file is created.
        date_str_format = str(datetime.date(datetime.now()))

        # this is to add 3 days to the current date - used in message to user
        days = str(datetime.date(datetime.today() + timedelta(days=3)))

        filename = scan_directory(date_str_format)
        if filename is not None:
            print("A file was already created for today's date (" + date_str_format + "). Please try again in 3 days (" + days + ").")
            return filename
    else:
        # If the function returns a filename that means the a file exists for the given date.
        filename = scan_directory(date)
        if filename is not None:
            print("File exists: " + filename)
            return filename

    # If we reach this point, the file doesn't exist.
    return None


def scan_directory(date):
    """ Scans the directory to find a file tagged with a given date.

    Parameters
    ----------
    date : str
        String representing the date needed to file within a filename.

    Returns
    -------
    filename : str
        Filename of existing html file.

    """

    directory = "local_html/"
    # Scans all the files within the local_html folder.
    for file in scandir(directory):
        filename = file.name
        # If index = -1 then that means the date wasn't found within the filename.
        index = filename.find(date)
        if index != -1:
            return filename

    # At this point, the file doesn't exist.
    return None


def verify_existing_file_in_3_days(date):
    """Verifies if a file for 3 days after the given date exists.


    Parameters
    ----------
    date : str
        Given date.

    Returns
    -------
    file_3_days : str
        Name of the file created 3 days after a given date.

    """

    # Converts the string into a datetime object. The time display is omitted from the datetime object.
    date_obj = datetime.date(datetime.strptime(date, "%Y-%m-%d"))
    # Add 3 days to the date object then convert it into a string.
    date_obj_3_days_str = str(date_obj + timedelta(days=3))
    # Verify if the file exists.
    file_3_days = scan_directory(date_obj_3_days_str)
    if file_3_days is not None:
        print("File for 3 days after exists: " + file_3_days)
        return file_3_days

    return None


def read_from_file(filename):
    """
    Reads a file's content and stores it in records.

    Parameters
    ----------
    filename: str
        name of the file to be read

    Returns
    -------
    records : str
        the content of the read file
    """
    try:
        fileobj = open(filename, "r")
        records = fileobj.read()
        fileobj.close()
        return records
    except IOError:
        print("File", filename, "does not exist.")
    finally:
        fileobj.close()


