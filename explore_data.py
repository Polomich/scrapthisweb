# -*- coding: utf-8 -*-
"""
@author: Rosemary Canon and Michelle Polonsky
"""

import fileIO_utils as IO
from scrape_utils import Scraped_Data
import db_functions as my_db
import matplotlib.pyplot as plt
import pandas as pd


def explore_data_main():
    """ Execution when the user decides to explore the data saved in a local html file. They will be asked to
    enter a date which will used to find a file created on that date. If a file exists, it searches if a file
    3 days later is created. If the second file doesn't exist, the program requests the user to try again with
    a different date.

    Returns
    -------
    None.

    """

    date = 0
    existing_file = None
    existing_file_3_days_after = None

    # Two things can happen at this point:
    # 1. The date entered is valid but is missing additional data (second file) to complete the 6 days of information.
    # 2. The date is invalid.

    while existing_file is None or existing_file_3_days_after is None:
        existing_file = None
        date = input("Enter a date in the following format YYYY-MM-DD: ")

        # At this point we now have to find if a file for the given date exists
        file_exists = IO.verify_existing_file(date)
        if file_exists is not None:
            existing_file = file_exists
        else:
            print("File for given date does not exists. Try again.")
            continue

        # Once here, we now have an existing file for the given date.
        # We will need to verify if a file created 3 days after the provided date exists.

        file_3_days = IO.verify_existing_file_in_3_days(date)
        if file_3_days is None:
            print("File for 3 days after given does not exist. Try again.")
            continue
        existing_file_3_days_after = file_3_days

    # At this point we can call a function to populate the database.
    # The following variables contain data for 6 consecutive days.
    day1 = Scraped_Data(existing_file).get_data()
    day2 = Scraped_Data(existing_file_3_days_after).get_data()
    # This variable contains the data for all 6 days.
    data_6_days = day1 + day2

    # at this point, we need to save the data within the database 
    db_login(data_6_days)


def db_login(data_6_days):
    """Asks the user to provide credentials to access their local database.

    Returns
    -------
    None.

    """
    print("The following credentials are needed to access your local database: ")
    localhost = input("Please enter your hostname ")
    username = input("Please enter your username ")
    password = input("Please enter your password ")

    # at this point we populate the db and create the tables for the neighboring countries 
    database = my_db.Database(localhost, username, password)
    con = database.db_connection()
    crsr = con.cursor()
    # create the database mipo_roca
    database.create_my_db(crsr)
    # now select the database
    database.select_db(crsr)
    database.create_table_corona(crsr)
    database.populate_corona_table(crsr, data_6_days)
    database.create_table_borders(crsr)
    con.commit()

    # at this point we can start the data analysis
    data_analysis(con, crsr)


def data_analysis(connection, cursor):
    """The functions to create 3 plots are called within this function.

    Parameters
    ----------
    connection : MySQLConnection
        Connection that is used to the database covid_corona_db_mipo_roca
    cursor : SQL cursor
        To pass into methods that execute SQL queries

    Returns
    -------
    None.

    """
    country = input("Please enter a country to analyze: ")
    create_6_day_evolution_on_country(country, connection)

    create_6days_evol_longest_border(country, connection, cursor)

    create_3days_comparison(country, connection, cursor)


def create_3days_comparison(country, connection, cursor):
    """
    Compares and analyses the difference between the current Deaths/1M pop for
    the first 3 days, for the country in question and at most 2 of its neighbours.

    :param country: str
        country entered by user to analyse and compare
    :param connection: MySQLConnection
        Connection that is used to the database covid_corona_db_mipo_roca
    :param cursor: SQL cursor
        To execute SQL queries
    :return:
        None.
    """
    country = country.lower()
    tablename = country.replace(" ", "") + "_borders_table"
    query_get_neighbors = "SELECT column_name from information_schema.columns " \
                          "WHERE table_name = '" + tablename + "';"
    cursor.execute(query_get_neighbors)
    # getting all the neighbors to the country
    neighbors_list_tuples = cursor.fetchall()
    # convert list of tuples to list of string
    neighbors_list = []
    for result in neighbors_list_tuples:
        neighbors_list.append(''.join(result[0]))

    # get borders length
    query = "select * from " + tablename + ";"
    cursor.execute(query)
    borders_list_tuple = cursor.fetchall()
    # sort the list or borders
    borders_sorted = sorted(borders_list_tuple[0])

    # get only the top 2 longest borders,
    # or in case there's only 1 neighbor, get them only.
    if len(borders_sorted) > 1:
        borders_top2 = [borders_sorted[len(borders_sorted) - 1], borders_sorted[len(borders_sorted) - 2]]
        # get 1st neighbor with longest border
        when_statement = ""
        for column in neighbors_list:
            when_statement = when_statement + "when " + column + " then '" + column + "' "
        query_longest_neighbor1 = "SELECT *, case " + str(borders_top2[0]) + " " + when_statement + \
                                  " else 'not found' end from " + tablename + ";"
        cursor.execute(query_longest_neighbor1)
        # get the query result as a list of 1 tuple
        neighbor_result = cursor.fetchall()
        # get only the country as a string
        neighbor1_alone = (neighbor_result[0][len(neighbor_result[0]) - 1]).lower()

        # get 2nd neighbor
        when_statement = ""
        for column in neighbors_list:
            when_statement = when_statement + "when " + column + " then '" + column + "' "
        query_longest_neighbor2 = "SELECT *, case " + str(borders_top2[1]) + " " + when_statement + \
                                  " else 'not found' end from " + tablename + ";"
        cursor.execute(query_longest_neighbor2)
        # get the query result as a list of 1 tuple
        neighbor_result = cursor.fetchall()
        # get only the country as a string
        neighbor2_alone = (neighbor_result[0][len(neighbor_result[0]) - 1]).lower()

        # get the count for half the days
        query_count = "SELECT count(*) from corona_table where country='" + country + "' or country='" \
                      + neighbor1_alone + "' or country='" + neighbor2_alone + "';"
        cursor.execute(query_count)
        count_list_tuple = cursor.fetchall()
        count_list = []
        for result in count_list_tuple:
            count_list.append(''.join(str(result[0])))
        # the result comes out as str, convert to int to divide,
        # the result is now a float, round the result to convert back to int
        half = round((int(count_list[0]))/2)

        # get the data
        query_get_3days = "SELECT Country_Date, Deaths_per_1M_pop from corona_table where country='" + country + \
                          "' or country='"+neighbor1_alone+"' or country='" + neighbor2_alone + "' " \
                          "order by country_date limit " + str(half) + ";"
        df = pd.read_sql_query(query_get_3days, connection)
        # plot the results
        colors = ['r', 'b', 'g']
        df.plot(x="Country_Date", y="Deaths_per_1M_pop",
                title="3 days deaths/1M pop comparison: "+country+" vs "+neighbor1_alone+" vs "+neighbor2_alone,
                kind="bar", color=colors)
        plt.show()

    elif len(borders_sorted) == 1:
        borders_top = borders_sorted[len(borders_sorted) - 1]
        # get neighbor with longest border
        when_statement = ""
        for column in neighbors_list:
            when_statement = when_statement + "when " + column + " then '" + column + "' "
        query_longest_neighbor = "SELECT *, case " + str(borders_top) + " " + when_statement + \
                                 " else 'not found' end from " + tablename + ";"
        cursor.execute(query_longest_neighbor)
        # get the query result as a list of 1 tuple
        neighbor_result = cursor.fetchall()
        # get only the country as a string
        neighbor_alone = (neighbor_result[0][len(neighbor_result[0]) - 1]).lower()

        # get the count for half the days
        query_count = "SELECT count(*) from corona_table where country='" + country + "' or country='" \
                      + neighbor_alone + "';"
        cursor.execute(query_count)
        count_list_tuple = cursor.fetchall()
        count_list = []
        for result in count_list_tuple:
            count_list.append(''.join(str(result[0])))
        half = round((int(count_list[0]))/2)

        # get the data
        query_get_3days = "SELECT Country_Date, Deaths_per_1M_pop from corona_table where country='" + country + \
                          "' or country='" + neighbor_alone + "' order by country_date limit " + str(half) + ";"
        df = pd.read_sql_query(query_get_3days, connection)
        # plot the results
        colors = ['r', 'b']
        df.plot(x="Country_Date", y="Deaths_per_1M_pop",
                title="3 days deaths/1M pop comparison: " + country + " vs " + neighbor_alone, kind="bar", color=colors)
        plt.show()


def create_6days_evol_longest_border(country, connection, cursor):
    """
    Compares and analyses the evolution through the 6 days of new cases for the country in question with the
    neighbour with the longest border.
    :param country: str
        country entered by user to analyse and compare
    :param connection: MySQLConnection
        Connection that is used to the database covid_corona_db_mipo_roca
    :param cursor: SQL cursor
        to execute queries
    :return:
        None.
    """
    country = country.lower()
    tablename = country.replace(" ", "") + "_borders_table"
    query_get_neighbors = "SELECT column_name from information_schema.columns " \
                          "WHERE table_name = '" + tablename + "';"
    cursor.execute(query_get_neighbors)
    # getting all the neighbors to the country
    neighbors_list_tuples = cursor.fetchall()
    # convert list of tuples to list of string
    neighbors_list = []
    for result in neighbors_list_tuples:
        neighbors_list.append(''.join(result[0]))
    # convert to on string
    neighbors_str = ','.join(neighbors_list)

    # get the longest border
    query_select_greatest = "SELECT greatest(" + neighbors_str + ") from " + tablename + ";"
    cursor.execute(query_select_greatest)
    border_list_tuples = cursor.fetchall()
    border_list = []
    for result in border_list_tuples:
        border_list.append(''.join(str(result[0])))
    border_str = ''.join(border_list)

    # get neighbor with longest border
    when_statement = ""
    for column in neighbors_list:
        when_statement = when_statement + "when " + column + " then '" + column + "' "
    query_longest_neighbor = "SELECT *, case " + border_str + " " + when_statement + \
                             " else 'not found' end from " + tablename + ";"
    cursor.execute(query_longest_neighbor)
    # get the query result as a list of 1 tuple
    neighbor_result = cursor.fetchall()
    # get only the country as a string
    neighbor_alone = neighbor_result[0][len(neighbor_result[0]) - 1]
    neighbor_alone = neighbor_alone.lower()

    # getting the data to plot
    query_data_country = "SELECT Country_Date, NewCases AS NewCases_" + country + " FROM corona_table " \
                         "WHERE Country = '" + country+"';"
    query_data_neighbor = "SELECT Country_Date, NewCases AS NewCases_" + neighbor_alone + " FROM corona_table " \
                          "WHERE Country = '" + neighbor_alone + "';"
    df_country = pd.read_sql_query(query_data_country, connection)
    df_neighbor = pd.read_sql_query(query_data_neighbor, connection)
    # combine both dataframes
    combine = [df_country, df_neighbor]
    df_result = pd.concat(combine)
    # plot the results
    df_result.plot(x="Country_Date", y=["NewCases_" + country, "NewCases_" + neighbor_alone],
                   title="6 days New Cases Comparison: " + country + " vs " + neighbor_alone, kind="bar")
    plt.show()


def create_6_day_evolution_on_country(country, connection):
    """Creates a bar plot based on three fields: NewCases, NewDeaths and
    NewRecovered. 
    

    Parameters
    ----------
    country : str
        Country that will be used for it's data.
    connection : MySQLConnection
        Connection that is used to the database covid_corona_db_mipo_roca

    Returns
    -------
    None.

    """
    query = 'SELECT Country_Date, NewCases, NewDeaths, NewRecovered FROM corona_table ' \
            'WHERE Country = \"' + country + '"'
    df = pd.read_sql_query(query, connection)
    df.plot(x="Country_Date", title="6 day evolution for " + country + " based on 3 indicators", kind="bar")
    plt.show()

# Note: some code is repeated in the functions. That is on purpose.
# The snippet of code could not be made into a new function by itself because numerous variables that are created
# within that snippet of code are needed later on.
