# -*- coding: utf-8 -*-
"""
@author: Rosemary Canon and Michelle Polonsky
"""

from scrape_utils import archive_data_from_webpage
import explore_data


def main_execution():
    choice = input("Please choose one of the following:\n "
                   "S: Scrape and save into database\n "
                   "X: Explore and analyse data\n")
    
    if choice.lower() == 's':
        archive_data_from_webpage()
    elif choice.lower() == 'x':
        explore_data.explore_data_main()
        

if __name__ == "__main__":
    main_execution()
