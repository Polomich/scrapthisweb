"""
@author: Michelle Polonsky and Rosemary Canon
1832416
"""
import mysql.connector
import fileIO_utils
import json


class Database:
    """
    A class used to connect to a database and create and manipulate its specific tables
    """
    def __init__(self, thehost, theuser, thepswd, thebase=None):
        """
        creates a database object

        Parameters:
            thehost : str
                the host of the database
            theuser : str
                the user of the database
            thepswd : str
                the password for the database
            thebase : str , optional
                the database name
        """
        self.__thehost = thehost
        self.__theuser = theuser
        self.__thepswd = thepswd
        self.__thebase = thebase

    def create_my_db(self, cursor):
        """
        creates a database called covid_corona_db_MIPO_ROCA

        Parameters:
            cursor : sql cursor
                to execute queries
        """
        query = "CREATE DATABASE IF NOT EXISTS covid_corona_db_MIPO_ROCA;"
        cursor.execute(query)
        self.__thebase = "covid_corona_db_MIPO_ROCA" 

    def db_connection(self):
        """
        creates a database connection

        Returns:
            a database connection
        """
        try:
            if self.__thebase is not None:
                return mysql.connector.connect(
                    host=self.__thehost,
                    user=self.__theuser,
                    password=self.__thepswd,
                    database=self.__thebase
                )
            else:
                return mysql.connector.connect(
                    host=self.__thehost,
                    user=self.__theuser,
                    password=self.__thepswd
                )
        except mysql.connector.Error as err:
            print('There is a problem with the database: {}'.format(err))

    def select_db(self, cursor):
        """
        selects a database

        Parameters
        ----------
        cursor : sql cursor
            to execute queries
        """
        query = "USE " + self.__thebase + ";"
        cursor.execute(query)

    def create_table_borders(self, cursor):
        """
        creates a table for each country with their borders and populates it

        Parameters
        ----------
        cursor : sql cursor
            to execute queries
        """
        data = fileIO_utils.read_from_file("country_neighbour_dist_file.json")
        countries_list = json.loads(data)

        for table in countries_list:
            countryname = next(iter(table))
            placeholders = ', '.join(['%s'] * len(table.get(countryname)))
            columns = ', '.join(table.get(countryname).keys())
            tablename = countryname + "_borders_table"
            values = table.get(countryname).values()
            values_list = list(values)
            createcolums = columns.replace(",", " int, ")
            createcolums = createcolums + " int"

            query_create = "CREATE TABLE IF NOT EXISTS %s ( %s );" % (tablename, createcolums)
            query_insert = "INSERT INTO %s ( %s ) VALUES ( %s );" % (tablename, columns, placeholders)

            cursor.execute(query_create)
            cursor.execute(query_insert, values_list)

    def create_table_corona(self, cursor):
        """
        creates a table called corona_table

        Parameters
        ----------
        cursor : sql cursor
            to execute queries
        """
        query = "CREATE TABLE IF NOT EXISTS corona_table (" \
                " Country_Date date NOT NULL," \
                " Country_Rank int NOT NULL," \
                " Country varchar(60) NOT NULL," \
                " TotalCases int," \
                " NewCases int," \
                " TotalDeaths int," \
                " NewDeaths int," \
                " TotalRecovered int," \
                " NewRecovered int," \
                " ActiveCases int," \
                " Serious_Critical int," \
                " Total_Cases_per_1M_pop int," \
                " Deaths_per_1M_pop int," \
                " TotalTests int," \
                " Tests_per_1M_pop int," \
                " Population int," \
                " Continent varchar(20)," \
                " 1_Case_Every_X_ppl int," \
                " 1_Death_Every_X_ppl int," \
                " 1_Test_Every_X_ppl int," \
                " PRIMARY KEY(Country_Date, Country))" 
        cursor.execute(query)
        
    def populate_corona_table(self, cursor, list_tuples):
        query = "INSERT INTO corona_table (" \
                "Country_Date, " \
                "Country_Rank, " \
                "Country, " \
                "TotalCases, " \
                "NewCases, " \
                "TotalDeaths, " \
                "NewDeaths, " \
                "TotalRecovered, " \
                "NewRecovered," \
                "ActiveCases, " \
                "Serious_Critical, " \
                "Total_Cases_per_1M_pop, " \
                "Deaths_per_1M_pop, TotalTests, " \
                "Tests_per_1M_pop," \
                "Population, Continent, " \
                "1_Case_Every_X_ppl, " \
                "1_Death_Every_X_ppl, " \
                "1_Test_Every_X_ppl) " \
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        cursor.executemany(query, list_tuples)
