# ScrapThisWeb

Web scraping and data analysis using Python - A pandemic case study by Naserddine Hallam 
<br>
_Project completed by Rosemary Canon (1512278) & Michelle Polonksy (1832416)_

This program will scrape data regarding coronavirus cases from https://www.worldometers.info/coronavirus/.

Upon runtime, the user is given two options: save today's data from worldometers or explore previous saved data.

If the user chooses the first option, the data from the webpage is saved as a local html file tagged with the current date in its filename. Each local file contains data for the current date and the two previous days. If a file was already created for the current date, the program will cease and request the user to try again on a later date. When the second option is chosen, the user has to enter a date that corresponds to a local html file. If the file does not exist, the program requests to try again with a different date. Once a file is retrieved, it then searches if a file exists **3 days later** after the given date. e.g. User enters 2021-03-24, the second file retrieved is 2021-03-27. The retrieval of the two html files contain 6 consecutive days worth of data. 

The user is then requested to input their login information to access their local database. This will allow to save the data within the two html files into a local database. Countries and the border lengths of their respectives neighboring countries will also be saved within the database. Once all the data is saved, the user is asked to enter a country name to analyze its data. 

The program finally outputs three plots: 
- The evolution of new cases, new deaths and new recovered through the 6 day period of the given country
- The evolution of new cases through the 6 day period of the given country and its neighboring country with the longest border
- The evolution of the current deaths/1M population for the first 3 days for the given country and two of its neighboring countries with the longest borders
